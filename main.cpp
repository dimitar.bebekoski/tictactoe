#include <iostream>
#include <string>
#include <vector>

void printS(const std::vector<std::string> &S, const char turn) {
  std::cout << "\033[2J\033[1;1H";
  std::cout << turn << "'s turn!\n";
  for (const auto &row : S)
    std::cout << row << "\n";
}

bool checkWin(const std::vector<std::string> &S, const char TURN) {
  return (S[0][0] == TURN && S[1][1] == TURN && S[2][2] == TURN) ||
         (S[0][2] == TURN && S[1][1] == TURN && S[2][0] == TURN) ||
         (S[0][0] == TURN && S[0][1] == TURN && S[0][2] == TURN) ||
         (S[1][0] == TURN && S[1][1] == TURN && S[1][2] == TURN) ||
         (S[2][0] == TURN && S[2][1] == TURN && S[2][2] == TURN) ||
         (S[0][0] == TURN && S[1][0] == TURN && S[2][0] == TURN) ||
         (S[0][1] == TURN && S[1][1] == TURN && S[2][1] == TURN) ||
         (S[0][2] == TURN && S[1][2] == TURN && S[2][2] == TURN);
}

char checkS(const std::vector<std::string> &S) {
  for (char turn : {'x', 'o'})
    if (checkWin(S, turn))
      return turn;

  return '-';
}
int main() {
  std::vector<std::string> S(3, "---");
  char turn = 'o';

  while (checkS(S) == '-') {
    turn = turn == 'o' ? 'x' : 'o';
    printS(S, turn);

    unsigned x, y;
    std::cin >> x >> y;

    if (S[x][y] != '-')
      continue;

    S[x][y] = turn;
  }

  std::cout << turn << " wins!" << std::endl;
  std::cin >> turn;
  return 0;
}
